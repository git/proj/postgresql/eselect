D := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
V := $(shell grep V $(D)/postgresql.eselect | grep -Po '[0-9.]+')

all:
	mkdir -p $(D)/eselect-postgresql-$(V)
	cp $(D)/postgresql.eselect $(D)/eselect-postgresql-$(V)/
	cd ${D} && tar cjf eselect-postgresql-$(V).tbz2 eselect-postgresql-$(V)

clean:
	rm $(D)/eselect-postgresql-$(V)/postgresql.eselect
	rmdir $(D)/eselect-postgresql-$(V)
	rm $(D)/eselect-postgresql-$(V).tbz2
